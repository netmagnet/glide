Glide
=====

Configuration
-------------
```
extensions:
    glide: Slts\Glide\DI\GlideExtension
    
glide:
    signkey: 'my-secret-sign-key' # recommend using a 128 character (or larger)
    servers:
        default: 
            config:
                source: null
                source_path_prefix: null
                cache: null
                cache_path_prefix: null
                group_cache_in_folders: null
                watermarks: null
                watermarks_path_prefix: null
                driver: null
                max_image_size: null
                defaults: null
                presets: null
                base_url: null
            autowired: false
    latteFilterGlobally: false # change to true if you want to have registered filter (see GenerateUrlFilter) for every instance of Latte from DI with default name, note that some server must be marked as autowired 
```