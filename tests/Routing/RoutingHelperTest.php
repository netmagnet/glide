<?php

namespace Slts\Glide\Tests;

use PHPUnit_Framework_TestCase;
use ReflectionClass;
use Slts\Glide\Routing\RoutingHelper;

class RoutingHelperTest extends PHPUnit_Framework_TestCase
{
    public function testGenerateRoutePattern()
    {
        $baseUrls = ['default' => 'img', 'advanced' => 'glide/', 'weird' => '/test/', 'none' => '', 'absolute' => 'http://example.com/glide'];
        $helper = new RoutingHelper($baseUrls);
        
        $r = new ReflectionClass($helper);
        $propertyReflection = $r->getProperty('baseUrls');
        $propertyReflection->setAccessible(true);
        $objectBaseUrls = $propertyReflection->getValue($helper);
        $this->assertEquals(['default' => 'img\/', 'advanced' => 'glide\/', 'weird' => '\/test\/', 'none' => '', 'absolute' => 'http\:\/\/example\.com\/glide\/'], $objectBaseUrls);
        
        $defaultParam = \Slts\Glide\Configuration::ROUTING_PARAM_NAME;
        $defaultExtension = 'jpg|jpeg|pjpg|pjpeg|png|gif';
        // key, paramName, expected
        $examples = [
            [null, null, [], "<{$defaultParam} .+\.[{$defaultExtension}].*>"],
            [null, null, ['jpg', 'png'], "<{$defaultParam} .+\.[jpg|png].*>"],
            ['unset', null, [], "<{$defaultParam} .+\.[{$defaultExtension}].*>"],
            [null, 'imgpath', [], "<imgpath .+\.[{$defaultExtension}].*>"],
            ['unset', 'imgpath', [], "<imgpath .+\.[{$defaultExtension}].*>"],
            ['default', null, [], "<path img\/.+\.[{$defaultExtension}].*>"],
            ['default', 'imgpath', [], "<imgpath img\/.+\.[{$defaultExtension}].*>"],
            ['advanced', null, [], "<path glide\/.+\.[{$defaultExtension}].*>"],
            ['weird', null, [], "<path \/test\/.+\.[{$defaultExtension}].*>"],
            ['none', null, [], "<path .+\.[{$defaultExtension}].*>"],
            ['absolute', null, [], "<path http\:\/\/example\.com\/glide\/.+\.[{$defaultExtension}].*>"],
        ];

        foreach ($examples as list($key, $paramName, $extensions, $expected)) {
            $this->assertSame($expected, $helper->generateRoutePattern($key, $paramName, $extensions));
        }
    }
}
