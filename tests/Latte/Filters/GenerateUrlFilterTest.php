<?php

namespace Slts\Glide\Tests;

use Mockery;
use PHPUnit_Framework_TestCase;
use Slts\Glide\Latte\Filters\GenerateUrlFilter;

class GenerateUrlFilterTest extends PHPUnit_Framework_TestCase
{
    public function testEngineRegister()
    {
        $urlBuilder = Mockery::mock(\League\Glide\Urls\UrlBuilder::class)->makePartial();
        $httpRequest = Mockery::mock(\Nette\Http\Request::class)
            ->shouldReceive('getUrl')->andReturnUsing(function () {
                return Mockery::mock(\Nette\Http\UrlScript::class)
                    ->shouldReceive('getBaseUrl')->andReturn('glide')
                    ->getMock()->makePartial()
                    ;
            })
            ->getMock()
            ->makePartial()
        ;

        $filter = new GenerateUrlFilter($urlBuilder, $httpRequest);
        $engine = new \Latte\Engine();
        $filter->engineRegister($engine);
        
        $this->assertArrayHasKey('imageurl', $engine->getFilters());
//        $this->assertEquals($filter, $engine->getFilters()['picurl']);
    }

    public function testTemplateRegister()
    {
        $latteFactory = Mockery::mock(\Nette\Bridges\ApplicationLatte\ILatteFactory::class)
            ->shouldReceive('create')->andReturnUsing(function () {
                return new \Latte\Engine();
            })
            ->getMock()
            ->makePartial();
        $templateFactory = new \Nette\Bridges\ApplicationLatte\TemplateFactory($latteFactory);
        $template = $templateFactory->createTemplate();
        
        $urlBuilder = Mockery::mock(\League\Glide\Urls\UrlBuilder::class)->makePartial();
        $httpRequest = Mockery::mock(\Nette\Http\Request::class)
            ->shouldReceive('getUrl')->andReturnUsing(function () {
                return Mockery::mock(\Nette\Http\UrlScript::class)
                    ->shouldReceive('getBaseUrl')->andReturn('glide')
                    ->getMock()->makePartial()
                    ;
            })
            ->getMock()
            ->makePartial()
        ;

        $filter = new GenerateUrlFilter($urlBuilder, $httpRequest);
        $filter->templateRegister($template);

        $this->assertArrayHasKey('imageurl', $template->getLatte()->getFilters());
//        $this->assertEquals($filter, $template->getLatte()->getFilters()['picurl']);
    }
    
}
