<?php

namespace Slts\Glide\Tests;

use PHPUnit_Framework_TestCase;
use Slts\Glide\Parameters\ParameterFilter;

class GlideParameterFilterTest extends PHPUnit_Framework_TestCase
{

    public function testFilter()
    {
        $filter = new ParameterFilter();
        foreach ($this->data() as list($source, $expected)) {
            self::assertEquals($expected, $filter->filter($source));
        }
    }

    public function data()
    {
        return [
            [['w' => 100], ['w' => 100]],
            [['w' => 100, 'path' => 'abc.jpg'], ['w' => 100]],
            [['w' => 100, 'abc' => 'kapr'], ['w' => 100]],
            [['abc' => 'kapr'], []],
        ];
    }
    
}
