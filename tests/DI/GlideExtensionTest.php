<?php

namespace Slts\Glide\Tests;

use League\Glide\Server;
use Nette\Bridges\HttpDI\HttpExtension;
use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use PHPUnit_Framework_TestCase;
use Slts\Flysystem\DI\FlysystemExtension;
use Slts\Glide\DI\GlideExtension;

class GlideExtensionTest extends PHPUnit_Framework_TestCase
{
    private function createContainer(string $config): Container
    {
        $loader = new ContainerLoader(sys_get_temp_dir(), true);
        $class = $loader->load(function (Compiler $compiler) use ($config) {
            $compiler->addExtension('http', new HttpExtension());
            $compiler->addExtension('flysystem', new FlysystemExtension());
            $compiler->addExtension('glide', new GlideExtension());
            $compiler->loadConfig($config);
        }, uniqid('', true));

        /** @var Container $container */
        $container = new $class();
        return $container;
    }

    public function testLoadConfigurationAllSetCorrectly()
    {
        $container = $this->createContainer(__DIR__ . '/configs/success.neon');

        self::assertInstanceOf(Server::class, $container->getService('glide.server.default'));
    }
}
