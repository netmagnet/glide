<?php

namespace Slts\Glide\Routing;

use Slts\Glide\Configuration;

class RoutingHelper
{
    private $baseUrls;

    public function __construct(array $baseUrls)
    {
        $this->baseUrls = array_map(function ($baseUrl){
            if (!$baseUrl) {
                return '';
            }
            $baseUrl = rtrim($baseUrl, '/') . '/';

            return preg_quote($baseUrl, '/');
        }, $baseUrls);
    }

    public function generateRoutePattern($serverName = null, $paramName = null, array $extensions = [])
    {
        $baseUrl = null !== $serverName && isset($this->baseUrls[$serverName]) ? $this->baseUrls[$serverName] : '';
        $paramName = $paramName ?: Configuration::ROUTING_PARAM_NAME;
        $extensions = $extensions ?: ['jpg', 'jpeg', 'pjpg', 'pjpeg', 'png', 'gif'];
        $joinedExtensions = implode('|', $extensions);

        return "<{$paramName} {$baseUrl}.+\.[{$joinedExtensions}].*>";
    }
}
