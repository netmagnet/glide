<?php

namespace Slts\Glide\Parameters;

class LatteParameterParser
{
    public static function parse(array $parameters)
    {
        $result = [];
        foreach ($parameters as $param) {
            list($key, $value) = explode('=', $param, 2);
            $result[$key] = $value;
        }

        return array_filter($result, function ($value){
            return null !== $value;
        });
    }
}
