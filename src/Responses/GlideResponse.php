<?php

namespace Slts\Glide\Responses;

use League\Flysystem\Handler;
use Nette\Application\IResponse;
use Nette\Http\IRequest;

class GlideResponse implements IResponse
{

    /** @var Handler */
    private $file;

    /** @var string */
    private $contentType;

    /** @var string */
    private $name;


    /**
     * @param Handler   $file        file path
     * @param string $name        imposed file name
     * @param string $contentType MIME content type
     * @param bool   $forceDownload
     */
    public function __construct(Handler $file, $name = null, $contentType = null)
    {
        $this->file = $file;
        $this->name = $name ?: basename($file->getPath());
        $this->contentType = $contentType ?: 'application/octet-stream';
    }


    /**
     * Returns the path to a downloaded file.
     * @return Handler
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Returns the file name.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Returns the MIME content type of a downloaded file.
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }


    /**
     * Sends response to output.
     *
     * @param IRequest              $httpRequest
     * @param \Nette\Http\IResponse $httpResponse
     *
     * @return void
     */
    public function send(IRequest $httpRequest, \Nette\Http\IResponse $httpResponse): void
    {
        $httpResponse->setContentType($this->contentType);

        $filesystem = $this->file->getFilesystem();
        $filesize = $length = $filesystem->getSize($this->file->getPath());

        $handle = $filesystem->readStream($this->file->getPath());

        $httpResponse->setHeader('Content-Length', $length);
        $httpResponse->setHeader('Pragma', null);
        $httpResponse->setHeader('Cache-Control', 'max-age=31536000, public');
        $httpResponse->setHeader('Expires', date_create('+1 years')->format('D, d M Y H:i:s').' GMT');

        while (!feof($handle) && $length > 0) {
            echo $s = fread($handle, min(4e6, $length));
            $length -= strlen($s);
        }
        fclose($handle);
    }

}
