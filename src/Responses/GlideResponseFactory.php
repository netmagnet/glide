<?php

namespace Slts\Glide\Responses;

use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use League\Glide\Responses\ResponseFactoryInterface;
use Nette\Application\BadRequestException;
use Slts\Glide\Exception\InvalidHandlerType;

class GlideResponseFactory implements ResponseFactoryInterface
{

    /**
     * Create response.
     *
     * @param  FilesystemInterface $cache Cache file system.
     * @param  string              $path  Cached file path.
     *
     * @return mixed The response object.
     * @throws BadRequestException
     * @throws FileNotFoundException
     * @throws InvalidHandlerType
     */
    public function create(FilesystemInterface $cache, $path)
    {
        if (!$cache->has($path)) {
            throw new FileNotFoundException($path);
        }

        $handler = $cache->get($path);
        if (!$handler->isFile()) {
            throw InvalidHandlerType::notFile($path);
        }

        return new GlideResponse($handler, null, $cache->getMimetype($path));
    }

}