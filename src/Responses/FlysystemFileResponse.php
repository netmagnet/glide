<?php

namespace Slts\Glide\Responses;

use League\Flysystem\Handler;
use Nette\Application\IResponse;
use Nette\Http\IRequest;

class FlysystemFileResponse implements IResponse
{

    /** @var Handler */
    private $file;

    /** @var string */
    private $contentType;

    /** @var string */
    private $name;

    /** @var bool */
    public $resuming = true;

    /** @var bool */
    private $forceDownload;


    /**
     * @param Handler   $file        file path
     * @param string $name        imposed file name
     * @param string $contentType MIME content type
     * @param bool   $forceDownload
     */
    public function __construct(Handler $file, $name = null, $contentType = null, $forceDownload = true)
    {
        $this->file = $file;
        $this->name = $name ?: basename($file->getPath());
        $this->contentType = $contentType ?: 'application/octet-stream';
        $this->forceDownload = $forceDownload;
    }


    /**
     * Returns the path to a downloaded file.
     * @return Handler
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Returns the file name.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Returns the MIME content type of a downloaded file.
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }


    /**
     * Sends response to output.
     *
     * @param IRequest              $httpRequest
     * @param \Nette\Http\IResponse $httpResponse
     *
     * @return void
     */
    public function send(IRequest $httpRequest, \Nette\Http\IResponse $httpResponse)
    {
        $httpResponse->setContentType($this->contentType);
        $httpResponse->setHeader('Content-Disposition',
            ($this->forceDownload ? 'attachment' : 'inline')
            . '; filename="' . $this->name . '"'
            . '; filename*=utf-8\'\'' . rawurlencode($this->name));
        
        $filesystem = $this->file->getFilesystem();
        $filesize = $length = $filesystem->getSize($this->file->getPath());

        $handle = $filesystem->readStream($this->file->getPath());

        if ($this->resuming) {
            $httpResponse->setHeader('Accept-Ranges', 'bytes');
            if (preg_match('#^bytes=(\d*)-(\d*)\z#', $httpRequest->getHeader('Range'), $matches)) {
                list(, $start, $end) = $matches;
                if ($start === '') {
                    $start = max(0, $filesize - $end);
                    $end = $filesize - 1;

                } elseif ($end === '' || $end > $filesize - 1) {
                    $end = $filesize - 1;
                }
                if ($end < $start) {
                    $httpResponse->setCode(416); // requested range not satisfiable
                    return;
                }

                $httpResponse->setCode(206);
                $httpResponse->setHeader('Content-Range', 'bytes ' . $start . '-' . $end . '/' . $filesize);
                $length = $end - $start + 1;
                fseek($handle, $start);

            } else {
                $httpResponse->setHeader('Content-Range', 'bytes 0-' . ($filesize - 1) . '/' . $filesize);
            }
        }

        $httpResponse->setHeader('Content-Length', $length);
        while (!feof($handle) && $length > 0) {
            echo $s = fread($handle, min(4e6, $length));
            $length -= strlen($s);
        }
        fclose($handle);
    }

}
