<?php

namespace Slts\Glide\Latte\Filters;

use Latte\Engine;
use League\Glide\Urls\UrlBuilder;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Http\Request;
use Slts\Glide\Parameters\LatteParameterParser;
use Slts\Glide\Parameters\ParameterFilter;

class GenerateUrlFilter
{
    protected static $filterName = 'imageurl';

    protected $urlBuilder;
    protected $baseUrl;

    public function __construct(UrlBuilder $builder, Request $httpRequest)
    {
        $this->urlBuilder = $builder;
        $this->baseUrl = rtrim($httpRequest->getUrl()->getBaseUrl(), '/');
    }

    public function engineRegister(Engine $engine)
    {
        $engine->addFilter(static::$filterName, $this);
    }

    public function templateRegister(Template $template)
    {
        $template->addFilter(static::$filterName, $this);
    }

    public function __invoke($path, ...$parameters)
    {
        $parsedParams = LatteParameterParser::parse($parameters);
        $filteredParams = ParameterFilter::filter($parsedParams);

        $url = $this->urlBuilder->getUrl($path, $filteredParams);

        return $this->baseUrl . $url;
    }
}
