<?php

namespace Slts\Glide\Exception;


class InvalidHandlerType extends \Exception
{

    public static function notDir($path)
    {
        return new static("Handler {$path} is not dir");
    }

    public static function notFile($path)
    {
        return new static("Handler {$path} is not file");
    }

}