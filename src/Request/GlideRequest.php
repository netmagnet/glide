<?php

namespace Slts\Glide\Request;

class GlideRequest
{
    /**
     * @var string $path
     */
    private $path;

    /**
     * @var array $parameters ;
     */
    private $parameters;

    /**
     * @param string $path
     * @param array  $parameters
     */
    public function __construct($path, array $parameters)
    {
        $this->path = $path;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
