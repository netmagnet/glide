<?php

namespace Slts\Glide\Request;

use League\Glide\Signatures\SignatureInterface;
use Nette\Application\Request;
use Slts\Glide\Parameters\ParameterFilter;

class GlideRequestFactory
{
    private $signature;

    public function __construct(SignatureInterface $signature)
    {
        $this->signature = $signature;
    }

    public function create(Request $request, $pathParameterKey)
    {
        $parameters = ParameterFilter::filter($request->getParameters());
        $path = $request->getParameter($pathParameterKey);
        $this->signature->validateRequest($path, $parameters);

        return new GlideRequest($path, $parameters);
    }
}
