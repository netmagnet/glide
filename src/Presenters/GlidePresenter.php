<?php

namespace Slts\Glide\Presenters;

use League\Glide\Server;
use League\Glide\Signatures\SignatureException;
use Nette\Application\BadRequestException;
use Nette\Application\IPresenter;
use Nette\Application\IResponse;
use Nette\Application\Request;
use Slts\Glide\Configuration;
use Slts\Glide\Request\GlideRequestFactory;

abstract class GlidePresenter implements IPresenter
{
    /**
     * @var Server $server
     */
    protected $server;

    /**
     * @var GlideRequestFactory $glideRequestFactory
     */
    protected $glideRequestFactory;

    public function injectGlideDeps(GlideRequestFactory $glideRequestFactory, Server $server)
    {
        $this->server = $server;
        $this->glideRequestFactory = $glideRequestFactory;
    }

    /**
     * @param Request $request
     *
     * @return IResponse
     * @throws BadRequestException
     */
    public function run(Request $request): IResponse
    {
        try{
            $glideRequest = $this->glideRequestFactory->create($request, Configuration::ROUTING_PARAM_NAME);

            return $this->server->getImageResponse($glideRequest->getPath(), $glideRequest->getParameters());

        } catch (SignatureException $exception){
            throw new BadRequestException(404); // 404?
        } catch (\Exception $exception){
            throw new BadRequestException(404); // 404?
        }
    }
}
