<?php

namespace Slts\Glide\DI;

use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Definition;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Nette\Utils\Strings;
use Slts\Glide\Latte\Filters\GenerateUrlFilter;
use Slts\Glide\Request\GlideRequestFactory;
use Slts\Glide\Responses\GlideResponseFactory;
use Slts\Glide\Routing\RoutingHelper;

class GlideExtension extends CompilerExtension
{
    private $generateUrlFilterDef;

    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'signkey' => Expect::string()->default(''),
            'latteFilterGlobally' => Expect::bool()->default(false),
            'servers' => Expect::arrayOf(
                Expect::structure([
                    'config' => Expect::structure([
                        'source' => Expect::mixed()->default(null),
                        'source_path_prefix' => Expect::mixed()->default(null),
                        'cache' => Expect::mixed()->default(null),
                        'cache_path_prefix' => Expect::mixed()->default(null),
                        'group_cache_in_folders' => Expect::bool()->default(true),
                        'watermarks' => Expect::mixed()->default(null),
                        'watermarks_path_prefix' => Expect::mixed()->default(null),
                        'driver' => Expect::string()->nullable()->default(null),
                        'max_image_size' => Expect::int()->nullable()->default(null),
                        'defaults' => Expect::array()->nullable()->default([]),
                        'presets' => Expect::array()->nullable()->default([]),
                        'base_url' => Expect::string()->nullable()->default(null),
                    ]),
                    'autowired' => Expect::mixed(false)
                ])
            ),
        ]);
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        if ($this->config->signkey) {
            $builder
                ->addDefinition($this->prefix('glideRequestFactory'))
                ->setFactory(GlideRequestFactory::class)
            ;

            $builder->addDefinition($this->prefix('signature'))
                ->setFactory('League\Glide\Signatures\SignatureFactory::create', [$this->config->signkey])
            ;
        }

        $netteResponseDef = $builder->addDefinition($this->prefix('netteFileResponse'))
            ->setFactory(GlideResponseFactory::class);

        $baseUrls = [];
        foreach ($this->config->servers as $name => $serverConf) {
            $serverConf->config->response = $netteResponseDef;

            $cnf = array_map(function ($item) use ($builder) {
                if (is_string($item) && Strings::startsWith($item, '@')) {
                    return $builder->getDefinition(Strings::substring($item, 1));
                }
                return $item;
            }, (array)(clone $serverConf->config));

            $serverDef = $builder->addDefinition($this->prefix("server.{$name}"))
                ->setFactory('League\Glide\ServerFactory::create', [$cnf]);
            $serverDef->setAutowired($serverConf->autowired);

            $urlBuilderDef = $builder->addDefinition($this->prefix("urlBuilder.{$name}"))
                ->setFactory('League\Glide\Urls\UrlBuilderFactory::create',
                    [$serverConf->config->base_url, $this->config->signkey])
                ->setAutowired($serverConf->autowired)
            ;

            $generateUrlFilterDef = $builder->addDefinition($this->prefix("generateUrlFilter.{$name}"))
                ->setFactory(GenerateUrlFilter::class)
                ->setArguments([$urlBuilderDef])
                ->setAutowired($serverConf->autowired)
            ;
            if ($serverConf->autowired) {
                $this->generateUrlFilterDef = $generateUrlFilterDef;
            }

            $baseUrls[$name] = $serverConf->config->base_url;
        }

        $builder->addDefinition($this->prefix('routingHelper'))
            ->setFactory(RoutingHelper::class, [$baseUrls]);

    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        if ($this->generateUrlFilterDef && $this->config->latteFilterGlobally) {
            $self = $this;

            $registerToLatte = function (Definition $def) use ($self) {
                $def->addSetup('?->engineRegister(?)', [$this->generateUrlFilterDef, '@self']);
            };

            $latteFactoryService = $builder->getByType(ILatteFactory::class) ?: 'nette.latteFactory';
            if ($builder->hasDefinition($latteFactoryService)) {
                $registerToLatte($builder->getDefinition($latteFactoryService));
            }

            if ($builder->hasDefinition('nette.latte')) {
                $registerToLatte($builder->getDefinition('nette.latte'));
            }
        }
    }

}